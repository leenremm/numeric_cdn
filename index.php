<!DOCTYPE html>
<!-- saved from url=(0037)https://lms.numeric.org/login/?next=/ -->
<html lang="en">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="img/numeric.ico">

    <title>Numeric Content Hotspot</title>

    <!-- Styles for plugins -->
    <link rel="stylesheet" href="css/style.css" type="text/css">

<style>
    body {
        padding-top: 25px;
        background: url("img/bg.jpg");
    }
</style>

<body class="nlms">
  <div class="wrapper">
    <div class="container" ui-view="">
      <div id="content" class="text-center">

        <a id="logo-lg" href=""><img src="img/logo.png"> </a>

        <div class="well well-transparent">
          <div id="logo-text">Little learning communities powered by great technology</div>
        </div>

        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-3 ">

            <div class="panel panel-nlms">
              <div class="panel-body">
                <h3>Video Download:</h3>
                <hr>
                <br><a href="content/video.m4v" download>Lesson on Multiplication (3.5Mb)</a>
              </div>
            </div>

            <div class="well well-transparent well-lg ft-wt">
              © 2014 Numeric.org All rights reserved
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
